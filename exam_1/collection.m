export collection, eq_collection

const collection <- immutable object collection
	export operation of[ElementType : AbstractType] -> [ret : NAT]
		forall AbstractType
		suchthat ElementType *> typeobject T
			operation =[T] -> [Boolean]
			operation asString[] -> [String]
		end T
		where NAT <- typeobject NAT
			operation empty[] -> [NA]
		end NAT
		where NA <- typeobject NA
			function getData[] -> [Array.of[ElementType]]
			operation add[ElementType] -> []
			function contains[ElementType] -> [Boolean]
			operation remove[ElementType] -> []
			function get[ElementType] -> [ElementType]
			operation asString[] -> [String]
			operation asLine[] -> [String]
		end NA

		ret <- immutable object aNAT
			export operation empty[] -> [result : NA]
				result <- object aNA
					var data : Array.of[ElementType] <- Array.of[ElementType].empty

					export function getData[] -> [r : Array.of[ElementType]]
						r <- data
					end getData

					export function contains[el : ElementType] -> [found : Boolean]
						found <- False
						for i : Integer <- data.lowerbound while i <= data.upperbound
								by i <- i + 1
							if el = data.getElement[i] then
								found <- True
							end if
						end for
					end contains

					export operation add[el : ElementType] -> []
						if ! self.contains[el] then
							data.addUpper[el]
						end if
					end add

					export operation remove[el : ElementType] -> []
						assert self.contains[el]
						for i : Integer <- data.lowerbound while 
								i <= data.upperbound by i <- i + 1
							if data.getElement[i] = el then
								data[i] <- data[data.upperbound]
								const x <- data.removeUpper
								exit
							end if
						end for
					end remove

					export function get[el : ElementType] -> [ret : ElementType]
						assert self.contains[el]
						for i : Integer <- data.lowerbound while 
								i <= data.upperbound by i <- i + 1
							if data.getElement[i] = el then
								ret <- data.getElement[i]
								exit
							end if
						end for
					end get

					export operation asString[] -> [ret : String]
						ret <- "[collection]\n"
						for i : Integer <- data.lowerbound while 
								i <= data.upperbound by i <- i + 1
							ret <- ret || "\t" || data.getElement[i].asString[] || "\n"
						end for
						ret <- ret || "[/collection]\n"
					end asString

					export operation asLine[] -> [ret : String]
						ret <- "["
						for i : Integer <- data.lowerbound while 
								i <= data.upperbound by i <- i + 1
							ret <- ret || " " || data.getElement[i].asString[] || ","
						end for
						ret <- ret || "]"
					end asLine

				end aNA
			end empty
		end aNAT
	end of
end collection


const eq_collection <- immutable object collection
	export operation of[ElementType : AbstractType] -> [ret : NAT]
		forall AbstractType
		suchthat ElementType *> typeobject T
		end T
		where NAT <- typeobject NAT
			operation empty[] -> [NA]
		end NAT
		where NA <- typeobject NA
			function getData[] -> [Array.of[ElementType]]
			operation add[ElementType] -> []
			function contains[ElementType] -> [Boolean]
			operation remove[ElementType] -> []
			function get[ElementType] -> [ElementType]
%			operation asString[] -> [String]
%			operation asLine[] -> [String]
		end NA

		ret <- immutable object aNAT
			export operation empty[] -> [result : NA]
				result <- object aNA
					var data : Array.of[ElementType] <- Array.of[ElementType].empty

					export function getData[] -> [r : Array.of[ElementType]]
						r <- data
					end getData

					export function contains[el : ElementType] -> [found : Boolean]
						found <- False
						for i : Integer <- data.lowerbound while i <= data.upperbound
								by i <- i + 1
							if el == data.getElement[i] then
								found <- True
							end if
						end for
					end contains

					export operation add[el : ElementType] -> []
						if ! self.contains[el] then
							data.addUpper[el]
						end if
					end add

					export operation remove[el : ElementType] -> []
						assert self.contains[el]
						for i : Integer <- data.lowerbound while 
								i <= data.upperbound by i <- i + 1
							if data.getElement[i] == el then
								data[i] <- data[data.upperbound]
								const x <- data.removeUpper
								exit
							end if
						end for
					end remove

					export function get[el : ElementType] -> [ret : ElementType]
						assert self.contains[el]
						for i : Integer <- data.lowerbound while 
								i <= data.upperbound by i <- i + 1
							if data.getElement[i] == el then
								ret <- data.getElement[i]
								exit
							end if
						end for
					end get

%					export operation asString[] -> [ret : String]
%						ret <- "[collection]\n"
%						for i : Integer <- data.lowerbound while 
%								i <= data.upperbound by i <- i + 1
%							ret <- ret || "\t" || data.getElement[i].asString[] || "\n"
%						end for
%						ret <- ret || "[/collection]\n"
%					end asString
%
%					export operation asLine[] -> [ret : String]
%						ret <- "["
%						for i : Integer <- data.lowerbound while 
%								i <= data.upperbound by i <- i + 1
%							ret <- ret || " " || data.getElement[i].asString[] || ","
%						end for
%						ret <- ret || "]"
%					end asLine

				end aNA
			end empty
		end aNAT
	end of
end collection

