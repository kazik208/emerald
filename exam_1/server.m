% Nopester server
export FileF, Server, FileWithPeersF, ServerT

const FileF <- object FileF
	export operation create[hash : Integer, name : String] -> [ret : FileT]
		ret <- object File
			export function getHash[] -> [ret : Integer]
				ret <- hash
			end getHash
			export function getName[] -> [ret : String]
				ret <- name
			end getName
			export operation =[other : FileT] -> [ret : Boolean]
				ret <- hash = other.getHash
			end =
			export operation asString[] -> [ret : String]
				ret <- "FileT Hash: " || hash.asString[]
					|| " Name: " || name
			end asString
		end File
	end create
end FileF

const FileWithPeersT <- typeobject FileWithPeersT
	function getF[] -> [FileT]
	function getFile[] -> [FileT]
	function listPeers[] -> [Array.of[ClientT]]
	function containsPeer[p : ClientT] -> [ret : Boolean]
	operation addPeer[p : ClientT] -> []
	operation removePeer[p : ClientT] -> []
	function =[FileWithPeersT] -> [Boolean]
	operation asString[] -> [String]
end FileWithPeersT

const FileWithPeersF <- object FileWithPeersF
	export operation create[file : FileT] -> [ret : FileWithPeersT]
		ret <- object FileWithNode
			const field f : FileT <- file
			const peers <- Eq_Collection.of[ClientT].empty

			export function getFile[] -> [ret : FileT]
				ret <- f
			end getFile
			export function listPeers[] -> [ret : Array.of[ClientT]]
				ret <- peers.getData[]
			end listPeers
			export function containsPeer[p : ClientT] -> [ret : Boolean]
				ret <- peers.contains[p]
			end containsPeer
			export operation addPeer[p : ClientT] -> []
				peers.add[p]
			end addPeer
			export operation removePeer[p : ClientT] -> []
				peers.remove[p]
			end removePeer
			export function =[other : FileWithPeersT] -> [ret : Boolean]
				ret <- f = other$f
			end =
			operation printNodes[arr : Array.of[ClientT]] -> [ret : String]
				ret <- ""
				for i : Integer <- arr.lowerbound while i <= arr.upperbound by i <- i+1
					ret <- ret || " " || arr[i]$name
				end for
			end printNodes
			export operation asString[] -> [ret : String]
				ret <- "FileWithPeersT File: " || f.asString[] 
					|| " Peers: " || self.printNodes[peers.getData[]]
			end asString
		end FileWithNode
	end create
	export operation fromHash[hash : Integer] -> [ret : FileWithPeersT]
		ret <- object FileWithNode
			const field f : FileT <- FileF.create[hash, ""]

			export function getFile[] -> [ret : FileT]
			end getFile
			export function listPeers[] -> [ret : Array.of[ClientT]]
			end listPeers
			export function containsPeer[p : ClientT] -> [ret : Boolean]
			end containsPeer
			export operation addPeer[p : ClientT] -> []
			end addPeer
			export operation removePeer[p : ClientT] -> []
			end removePeer
			export function =[other : FileWithPeersT] -> [ret : Boolean]
				ret <- f = other$f
			end=
			export operation asString[] -> [ret : String]
				ret <- "FileWithPeersT File: " || f.asString[]
			end asString
		end FileWithNode
	end fromHash
end FileWithPeersF

const ServerT <- typeobject ServerT
	function listFiles[] -> [Array.of[FileT]]
	function listNodes[FileT] -> [Array.of[ClientT]]
	operation addFile[FileT, ClientT] -> []
	operation removeFile[FileT, ClientT] -> []
	operation dump_files[] -> [ret : String]
end ServerT

const Server : ServerT <- object Server
	const files <- Collection.of[FileWithPeersT].empty
	const peers <- Eq_Collection.of[ClientT].empty
	const lock <- LockF.create[]

	% file access
	function hasFile[hash : Integer] -> [ret : Boolean]
		const file <- FileWithPeersF.fromHash[hash]
		ret <- files.contains[file]
	end hasFile
	function getFileByHash[hash : Integer] -> [ret : FileWithPeersT]
		assert self.hasFile[hash]
		const file <- FileWithPeersF.fromHash[hash]
		ret <- files.get[file]
	end getFileByHash
	% Peer access
	function hasPeer[p : ClientT] -> [ret : Boolean]
		ret <- peers.contains[p]
	end hasPeer
	function getPeerByNode[p : ClientT] -> [ret : ClientT] 
		% if there is a peer, get it,
		% if there is no create and insert into collections
		if ! self.hasPeer[p] then
			peers.add[p]
		end if
		assert peers.contains[p]
		ret <- peers.get[p]
	end getPeerByNode
	

	export function listFiles[] -> [ret : Array.of[FileT]]
		const files_arr <- files.getData
		ret <- Array.of[FileT].empty
		for i : Integer <- files_arr.lowerbound while i <= files_arr.upperbound
				by i <- i + 1
			ret.addUpper[files_arr.getElement[i].getFile[]]
		end for
		unavailable
			(locate self)$stdout.putString["listing failed\n"]
		end unavailable
	end listFiles

	export function listNodes[file : FileT] -> [ret : Array.of[ClientT]]
		if ! self.hasFile[file$hash] then
			(locate self)$stdout.putString["does not have a file! " || file.asstring || "\n"]
		else
			ret <- self.getFileByHash[file$hash].listPeers[]
		end if
	end listNodes

	
	export operation addFile[file : FileT,  peer : ClientT] -> []
		(locate self)$stdout.putString["Adding file" || file.asString || " on: " || peer$name || "\n"]
		lock.acquire[]
		const hash : Integer <- file.getHash[]
		% this will add new clients
		const _ <- self.getPeerByNode[peer]
		if self.hasFile[hash] then
			(locate self)$stdout.putString["Adding file - another copy \n"]
			% add one node for this file
			const fileWithPeers <- self.getFileByHash[hash]
			% check that the file was not already added
			if ! fileWithPeers.containsPeer[peer] then
				(locate self)$stdout.putString["Adding file - new for this peer \n"]
				fileWithPeers.addPeer[peer]
			end if
		else
			% add a new file
			const fileWithPeers <- FileWithPeersF.create[file]
			fileWithPeers.addPeer[peer]
			files.add[fileWithPeers]
		end if
		lock.release[]
		unavailable
			lock.release[]
		end unavailable
	end addFile

	export operation removeFile[file : FileT, peer : ClientT] -> []
		lock.acquire[]
		const hash : Integer <- file.getHash[]
		if self.hasFile[hash] then
			% add one node for this file
			const fileWithPeers <- self.getFileByHash[hash]
			% check that the file was not already added
			if fileWithPeers.containsPeer[peer] then
				fileWithPeers.removePeer[peer]
				% remove file if it has no peers
				if fileWithPeers.listPeers[].empty[] then
					files.remove[fileWithPeers]
				end if
			end if
		end if
		lock.release[]
	end removeFile

	export operation dump_files[] -> [ret : String]
		const peer_arr <- peers.getData[]
		ret <- ""
		for i : Integer <- peer_arr.lowerbound[] while i <= peer_arr.upperbound[] by i <- i + 1
			begin
				ret <- ret || " " || peer_arr[i]$name
				unavailable
				end unavailable
			end
		end for
		ret <- ret || "\n" 
		ret <- ret || files.asString[]

	end dump_files
end Server
