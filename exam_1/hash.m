% Bernstein hash implementation
export hasherT, bern_hasher

const hasherT <- immutable typeobject hasherT
	function getHash[String] -> [Integer]
end hasherT

const bern_hasher : hasherT <- immutable object hasher
	export function getHash[s : String] -> [hash : Integer]
		% stdout.putString[s || "\n"]
		hash <- 0
		for i : Integer <- 0 while i < s.length by i <- i + 1
			% stdout.putString[s.getElement[i].asString || "\n"]
			hash <- (33 * hash + (s.getElement[i]).ord)
		end for
	end getHash
end hasher

