const nopester <- object nopester
	var serverNode : Node <- locate server
	const clientF <- ClientFF.hash[bern_hasher, server]
	const names <- {"red", "green", "blue", "violet", "cyan", "magenta", "black", "white"}
	var name_pos : Integer <- 0
	const songs <- {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"}

	operation next_cl_name[] -> [name : String]
		name <- names[name_pos] || name_pos.asString
		name_pos <- name_pos + 1
		if name_pos > names.upperbound then
			name_pos <- 0
		end if
	end next_cl_name

	operation init_client[client : ClientT] -> []
		for i : Integer <- songs.lowerbound while (i <= songs.upperbound and i <= name_pos * 3) by i <- i + 1
			var name : String <- songs[i]
			var contents : String <- name || name || name
			client.create_file[name, contents]
		end for
	end init_client

	operation initialize[] -> []
		stdout.putString["nopester loading...\n"]
		var nodes : NodeList <- (locate self).getactivenodes

		for i : Integer <- nodes.lowerbound while i <= nodes.upperbound by i <- i + 1
			var nod : Node <- nodes[i]$thenode

			if nod$lnn == serverNode$lnn then
				stdout.putString["Not adding client on server node\n"]
			else
				var name : String <- self.next_cl_name[]
				stdout.putString["creating client: "|| name || "\n"]
				var client : ClientT <- clientF.create[name]
				move client to nod
				self.init_client[client]
				client.shell[]
			end if
		end for
	end initialize
	

	process
		self.initialize[]
	end process
end nopester
