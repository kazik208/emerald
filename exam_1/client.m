% nopester client
export CFileFF, CFileT, ClientFF

const CFileT <- typeobject CFileT
	function getHash[] -> [ret : Integer]
	function getName[] -> [ret : String]
	function getContents[] -> [ret : String]
	function =[CFileT] -> [ret : Boolean] 
	operation asString[] -> [String]
end CFileT

const CFileTF <- typeobject CFileTF
	operation create[name : String, contents : String] -> [ret : CFileT]
end CFileTF

const CFileFF <- object CFileFF
	export operation hash[hasher : hasherT] -> [ret : CFileTF]
		ret <- object CFileF
			export operation create[name : String, contents : String] -> [ret : CFileT]
				ret <- immutable object newFile
					const h : Integer <- hasher.getHash[contents]
					export function getHash[] -> [ret : Integer]
						ret <- h
					end getHash
					export function getName[] -> [ret : String]
						ret <- name
					end getName
					export function getContents[] -> [ret : String]
						ret <- contents
					end getContents
					export function =[other : CFileT] -> [ret : Boolean]
						ret <- h = other.getHash[]
					end =
					export operation asString[] -> [ret : String]	
						ret <- "F: " || name || " H: " || h.asString || " [" || contents || "]"
					end asString
				end newFile
			end create
		end CFileF
	end hash
	export operation fake_file[h : Integer, n : String] -> [ret : CFileT]
		ret <- immutable object newFile
			export function getHash[] -> [ret : Integer]
				ret <- h
			end getHash
			export function getName[] -> [ret : String]
				ret <- n
			end getName
			export function getContents[] -> [ret : String]
				ret <- ""
			end getContents
			export function =[other : CFileT] -> [ret : Boolean]
				ret <- h = other.getHash[]
			end =
			export operation asString[] -> [ret : String]	
				ret <- "FAKE FILE: " || n || " H: " || h.asString || " [" || "]"
			end asString
		end newFile
	end fake_file
end CFileFF


const ClientFT <- typeobject ClientFT
	operation create[name : String] -> [ClientT]
end ClientFT

const ClientFF <- object ClientFF
 export operation hash[hasher : hasherT, server : ServerT] -> [ret : ClientFT]
  ret <- object ClientF
   export operation create[name : String] -> [ret : ClientT]
    ret <- object Client
	const files <- Collection.of[CFileT].empty
	const cfileF <- CFileFF.hash[hasher]

	export operation create_file[name : String, contents : String] -> []
		const file <- cfileF.create[name, contents]
		(locate self)$stdout.putString["creating file " || file.asString || "\n"]
		files.add[file]
		server.addFile[FileF.create[file$hash, file$name], self]
	end create_file

	export operation remove_file[hash : Integer] -> []
		const file <- CFileFF.fake_file[hash, ""]
		if ! files.contains[file] then
			(locate self)$stdout.putString["Don't have this file " || file.asString || "\n"]
		else
			files.remove[file]
			server.removefile[FileF.create[file$hash, file$name], self]
		end if
	end remove_file

	export operation deliver_file[hash : Integer] -> [contents : String]
		const fake_file <- CFileFF.fake_file[hash, ""]
		const file <- files.get[fake_file]
		contents <- file.getContents[]
	end deliver_file

	export operation list_local_files[] -> [ret : Array.of[FileT]]
		ret <- Array.of[FileT].empty
		const files_data <- files.getData[]
		for i : Integer <- files_data.lowerbound 
				while i <= files_data.upperbound  by i <- i + 1
			var hash : Integer <- files_data[i].getHash[]
			var name : String <- files_data[i].getName[]
			ret.addUpper[FileF.create[hash, name]]
		end for
	end list_local_files

	export operation list_all_files[] -> [ret : Array.of[FileT]]
		ret <- server.listFiles[]
	end list_all_files

	export operation getName[] -> [ret : String]
		ret <- name
	end getName

	export operation shell[] -> []
		const my_client <- self
		const shell <- object shell
		process
		(locate self)$stdout.putstring["Shell started\n"]
		loop
			begin
			(locate self)$stdout.putstring["q - list local files\n"]
			(locate self)$stdout.putstring["w - list all files\n"]
			(locate self)$stdout.putstring["e - create new file\n"]
			(locate self)$stdout.putstring["r - remove local file\n"]
			(locate self)$stdout.putstring["t - read file\n"]
			(locate self)$stdout.putstring["y - dump server state\n"]

			var input : Character <- ((locate self)$stdin.getstring)[0]

			if input = 'q' then
				(locate self)$stdout.putstring["listing local files\n"]
				const local_list <- my_client.list_local_files[]
				for i : Integer <- local_list.lowerbound while i <= local_list.upperbound by i <- i + 1
					(locate self)$stdout.putstring[local_list[i].asString[] || "\n"]
				end for
			elseif input = 'y' then
				(locate self)$stdout.putstring["dumping server state\n"]
				(locate self)$stdout.putstring[server.dump_files[]]
			elseif input = 'w' then
				(locate self)$stdout.putstring["listing all files\n"]
				const files <- server.listFiles[]
				var print : String <- ""
				for i : Integer <- files.lowerbound while i <= files.upperbound by i <- i + 1
					print <- print || files[i].asstring || "\n"
				end for
				(locate self)$stdout.putstring[print]
			elseif input = 'e' then
				(locate self)$stdout.putstring["creating new file\n"]
				(locate self)$stdout.putstring["Name?\n"]
				var name : String <- (locate self)$stdin.getstring
				(locate self)$stdout.putstring["Content?\n"]
				var content : String <- (locate self)$stdin.getstring
				my_client.create_file[name, content]
			elseif input = 'r' then
				(locate self)$stdout.putstring["removing local file\n"]
				(locate self)$stdout.putstring["Hash?\n"]
				var hashStr : String <- (locate self)$stdin.getstring
				var hash : Integer <- Integer.literal[hashStr]
				my_client.remove_file[hash]
			elseif input = 't' then
				begin
				(locate self)$stdout.putstring["reading a file\n"]
				(locate self)$stdout.putstring["Hash?\n"]
				var hashStr : String <- (locate self)$stdin.getstring
				var hash : Integer <- Integer.literal[hashStr]
				const file <- FileF.create[hash, ""]
				(locate self)$stdout.putstring["Listing nodes\n"]
				var clients : Array.of[ClientT] <- server.listNodes[file]
				(locate self)$stdout.putstring["nodes listed\n"]
				var print : String <- ""
				var contents : String <- "No working client found"
				(locate self)$stdout.putstring["starting iterate"]
				for i : Integer <- clients.lowerbound while i <= clients.upperbound by i <- i + 1
					begin
						contents <- clients[i].deliver_file[hash]
						exit
						% if unavailable client, go to the next one
						unavailable
							(locate self)$stdout.putstring["remote client crashed"]
						end unavailable
					end
				end for
				(locate self)$stdout.putstring["Contents: [" || contents || "]\n"]
				end
			end if
			unavailable				
				(locate self)$stdout.putstring["something went wrong\n"]
			end unavailable
			end
		end loop
		end process

		end shell
	end shell

    end Client
   end create
  end ClientF
 end hash
end ClientFF

