const NS_tester <- object test
	process
		const ns <- NameServerF.empty[]
		const name1 <- "A"
		var ret, ret2 : Any
		
		ns.addObject[name1, name1]
		
		ret <- ns.getObject[name1]
		assert ! ret == nil
		ret2 <- ns.getObject["A"]
		assert ret2 == ret
		ret <- ns.getObject["B"]
		assert ret == nil
		
		const ns2 <- ns.CloneMe[]
		
		ns.addObject["C", name1]
		ret <- ns2.getObject[name1]
		assert ! ret == nil
		ret2 <- ns2.getObject["A"]
		assert ret2 == ret
		ret <- ns2.getObject["B"]
		assert ret == nil
	end process
end test
