const timer_test <- object timer_test
	process
		const timerA <- TimerF.create[]
		const time1 : Time <- timerA.getTime[]
		stdout.putString["time A: " || time1.asString || "\n"]
		const timerB : TimerT <- timerA.cloneMe[]
		const time2 : Time <- timerB.getTime[]
		stdout.putString["time B: " || time2.asString || "\n"]
	end process
end timer_test
