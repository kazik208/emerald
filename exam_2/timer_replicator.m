export timer_replicator

% this is an example of using a replication framework to keep track fo time
% there are functions get/setName, to show how to use a framework with changing objects as well

const timer_replicator <- object timer_replicator	
	% This will return a reference wrapper for all the timers which will be replicated
	export operation replicateOriginal[original : TimerT, count : Integer] -> [timer_wrapper : TimerT]
		timer_wrapper <- object timer_wrapper
			const replF <- replicatorFramework.of[TimerT]
			
			initially
				(locate self)$stdout.putstring["init\n"]
				replF.init[original, count]
			end initially
			
			export operation getTime[] -> [ret : Time]
				ret <- replF$primary.getTime[]
			end getTime
			
			export operation getName[] -> [ret : String]
				ret <- replF$primary.getName[]
			end getName
			
			export operation setName[n : String] -> []
				% update primary
				replF$primary.setName[n]
				% update the clones
				const clones <- replF$clones
				for i : Integer <- clones.lowerbound while i <= clones.upperbound by i <- i + 1
					begin
						% update observers
						clones[i].setName[n]
						unavailable
							replF.print["one of copies not working - ignoring"]
						end unavailable
					end
				end for
			end setName
		
			export operation moveMe[Node] -> []
				% this should never be called!!
				assert False 
			end moveMe
		
			export operation cloneMe[] -> [ret : TimerT]
				% this should never be called!!
				assert False 
			end cloneMe
		end timer_wrapper
	end replicateOriginal
	
end timer_replicator
