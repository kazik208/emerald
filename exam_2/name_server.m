% name server for keeping objects
export NameServerT, NameServerF

const NameServerT <- typeobject NameServerT
	operation setValues[Array.of[Any], Array.of[String]] -> []
	operation getNames[] -> [Array.of[String]]
	operation addObject[String, Any] -> []
	operation getObject[String] -> [Any]
	operation moveMe[Node] -> []
	operation cloneMe[] -> [NameServerT]
end NameServerT

const NameServerF <- object NameServerF
	% creates a nameserver with objects supplied
	export operation empty[] -> [nameserver : NameServerT]
	nameserver <- object NameServer
		const objects <- Array.of[Any].empty
		const names <- Array.of[String].empty
		
		export operation moveMe[n : Node] -> []
			move names to n
			move objects to n
			move self to n
		end moveMe
		
		export operation setValues[obiekty : Array.of[Any], nazwy : Array.of[String]] -> []
			for i : Integer <- 0 while i <= obiekty.upperbound - obiekty.lowerbound by  i <- i + 1
				objects.addUpper[obiekty[obiekty.lowerbound + i]]
				names.addUpper[nazwy[nazwy.lowerbound + i]]
			end for
		end setValues
		
		
		% get all registered names
		export operation getNames[] -> [ret : Array.of[String]]
			ret <- names
		end getNames
		
		% add new object with a given name
		export operation addObject[name : String, value : Any] -> []
			objects.addUpper[name]
			names.addUpper[name]
		end addObject
		
		% get object with a specified name
		export operation getObject[name : String] -> [ret : Any]
			ret <- nil
			for i : Integer <- names.lowerbound while i <= names.upperbound by i <- i + 1
				if names[i] = name then
					ret <- objects[i]
					exit
				end if
			end for
		end getObject
		
		% get a copy of current name server
		export operation cloneMe[] -> [ret : NameServerT]
			ret <- NameServerF.empty[]
			ret.setValues[objects, names]
		end cloneMe
		
	end NameServer
	end empty
	
end NameServerF
