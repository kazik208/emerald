const timerepl_tester <- object tester
	operation print[msg : String] -> []
		stdout.putString[msg || "\n"]
	end print
	process
		(locate self).delay[Time.create[1, 0]]
		const orig <- TimerF.create[]
		const manager <- timer_replicator.replicateOriginal[orig, 3]
		manager.setName["a"]
		loop
			const curr_time <- manager.getTime[]
			const name <- manager.getName[]
			self.print["got name : "|| name || " time " || curr_time.asString[]]
			manager.setName[curr_time.asDate]
			(locate self).delay[Time.create[1, 0]]
		end loop
		
	end process
end tester
