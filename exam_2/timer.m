export TimerT, TimerF

const TimerT <- typeobject TimerT
	operation getTime[] -> [Time]
	operation setName[String] -> []
	operation getName[] -> [String]
	operation moveMe[Node] -> []
	operation cloneMe[] -> [TimerT]
end TimerT

const TimerF <- object TimerF
	export operation create[] -> [timer : TimerT]
		timer <- object timer
			field name : String
			
			export operation getTime[] -> [ret : Time]
				const here <- locate self
				ret <- here.getTimeofDay
			end getTime
			
			export operation moveMe[n : Node] -> []
				move self to node
			end moveMe
			
			export operation cloneMe[] -> [ret : TimerT]
				ret <- TimerF.create[]
				ret.setName[name]
			end cloneMe
		end timer
	end create
end TimerF
