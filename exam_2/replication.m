export replicatedT

const replicatedT <- typeobject replicatedT
	operation cloneMe[] -> [replicatedT]
end replicatedT

const replicatorT <- typeobject replicatorT
	operation putOriginal[T] -> [T]
	forall T
	% below should be T *> replicatedT,
	% but there is no way to obtain that
	suchthat T *> typeobject NAT
		operation cloneMe[] -> [T]
	end NAT
end replicatorT
