export replicatedT, replicatorFramework

% This is a generic replicator framework, to create a replicator one should
% do it in a manner similar to timer_replicator - use replicator framework
% for all ahndling of the copies, and just create object with the same signature
% as copied obiects.
% External functions (from signature of the object) should follow a simple scheme:
% Below assumes that replication framework is available as replF
% - all functions doing only get and no changes to object 
%   (hence can be called only on primary copy) should look like:
%      	    export operation getName[] -> [ret : String]
%               ret <- replF$primary.getName[]
%           end getName
%
% - all functions making changes to the object should look like:
%
%           export operation setName[n : String] -> []
%               % update primary
%               replF$primary.setName[n]
%               const clones <- replF$clones
%               for i : Integer <- clones.lowerbound while i <= clones.upperbound by i <- i + 1
%               begin
%                       % update observers
%                       clones[i].setName[n]
%                       unavailable
%                            replF.print["one of copies not working - ignoring"]
%                       end unavailable
%               end
%               end for
%            end setName
%
% Operation replicateOriginal[] gives a manager for handling the copies.

const replicatedT <- typeobject replicatedT
	operation cloneMe[] -> [replicatedT]
end replicatedT

const replicatorFramework <- object replicatorF
	export operation of[ElemType : AbstType] -> [ret : NAT]
		forall AbstType
		suchthat ElemType *> typeobject replicatedT
			operation cloneMe[] -> [replicatedT]
			operation moveMe[Node] -> []
		end replicatedT
		where NAT <- typeobject NAT
			operation init[ElemType, Integer] -> []
			operation getPrimary[] -> [ElemType]
			operation getClones[] -> [Array.of[ElemType]]
			operation print[msg : String] -> []
			operation printThere[n : Node, msg : String] -> []
			operation restoreBalance[] -> []
		end NAT
		
		ret <- object replF
			% primary copy
			var primary : ElemType
			% how many copies to keep
			var count : Integer
			% additional copies
			var clones : array.of[ElemType] <- array.of[ElemType].empty
			
			export operation getPrimary[] -> [ret : ElemType]
				self.restoreBalance
				ret <- primary
				unavailable
					ret <- self.getPrimary[]
				end unavailable
			end getPrimary
			
			export operation getClones[] -> [ret : Array.of[ElemType]]
				self.restoreBalance
				ret <- clones
				unavailable
					ret <- self.getClones[]
				end unavailable
			end getClones
			
			export operation init[el : ElemType, n : Integer] -> []
				self.print["initialising"]
				primary <- el
				count <- n
				self.replicateClones[]
				unavailable
					self.print["initial replication failed badly"]
				end unavailable
			end init
			
			export operation print[msg : String] -> []
				(locate self)$stdout.putString[msg || "\n"]
			end print
			
			export operation printThere[n : Node, msg : String] -> []
				n$stdout.putString[msg || "\n"]
			end printThere
						
			% remove clone reference
			export operation removeClone[idx : Integer] -> []
				clones[idx] <- clones[clones.upperbound]
				const _ <- clones.removeUpper[]
			end removeClone
			
			% check if a copy is working
			export operation isWorking[copy : replicatedT] -> [ret : Boolean]
				ret <- True
				const clone <- copy.cloneMe[]
				unavailable
					ret <- False
				end unavailable
			end isWorking
			
			% promote one of the cloned copies to become the new master
			export operation promoteClone[] -> []
				var found : Boolean <- False
				% first find working clone
				for i : Integer <- clones.lowerbound 
						while i <= clones.upperbound
						by i <- i + 1
					begin
						const clone : ElemType <- clones[i]
						% check that it is working
						if self.isWorking[clone] then
							primary <- clone
							% working clone found, remove from array and exit
							self.removeClone[i]
							found <- True
							exit
						end if
					end
				end for
				if ! found then
					self.print["no working copy found!!!!"]
				else 
					self.print["working copy found and promoted!"]
				end if
				unavailable
					self.print["promote clone unavailable"]
				end unavailable
			end promoteClone
			
			% check if there are any missing clones
			export operation missingClones[] -> [ret : Boolean]
				var workingClones : Integer <- 0
				for i : Integer <- clones.lowerbound 
						while i <= clones.upperbound
						by i <- i + 1
					begin
						const clone : replicatedT <- clones[i]
						% check that it is working
						if self.isWorking[clone] then
							workingClones <- workingClones + 1
						end if
					end
				end for
				if workingClones < count - 1 then
					ret <- True
				else
					ret <- False
				end if
				unavailable
					self.print["missing clones unavailable"]
					ret <- True
				end unavailable
			end missingClones
			
			export operation replicateClones[] -> []
				assert ! primary == nil
				self.print["replicating clones"]
				% This is a simple replication scheme - replicate onto different nodes using the primary copy
				% guarantees: there will be copies on separate nodes, as long as primary does not fail during the process
				const here : Node <- locate self
				const nodes <- here$ActiveNodes
				% throw out all the old clones
				clones <- Array.of[ElemType].empty
				var toCopy : Integer <- count

				for i : Integer <- nodes.lowerbound 
						while i <= nodes.upperbound and toCopy > 0
						by i <- i + 1
					begin
						const there : Node <- nodes[i]$theNode
						% Do not keep a copy here, only there :)
						if ! there == here then
							if locate primary == here then
								self.print["moving primary copy from here "|| toCopy.asString]
								primary.moveMe[there]
								move primary to there
								self.printThere[there, "moved primary copy here "|| toCopy.asString]
							else
								self.print["creating copy "|| toCopy.asString]
								const clone <- primary.cloneMe[]
								clone.moveMe[there]
								move clone to there
								self.printThere[there, "moved copy there "|| toCopy.asString]
								clones.addUpper[clone]
							end if
							toCopy <- toCopy - 1
						end if
						unavailable
							self.print["one of the nodes failed - ignoring"]
						end unavailable
					end
				end for
				unavailable
					self.print["Something failed badly"]
				end unavailable
			end replicateClones
			
			
			% make sure the primary is working and that there are enough clones
			export operation restoreBalance[] -> []
				% first fix the primary if necessary
				if ! self.isWorking[primary] then
					self.print["primary not working"]
					self.promoteClone[]
				end if
				% now fix missing clones
				if self.missingClones[] then
					self.print["missing clones"]
					% make sure there are enough copies
					self.replicateClones[]
				end if
				unavailable
					self.print["restore balance unavailable"]
				end unavailable
			end restoreBalance
			
		end replF
	end of
	
end replicatorF
