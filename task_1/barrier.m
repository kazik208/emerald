const barrierExample <- object barrierExample
	const Barrier <- monitor class Barrier [size : Integer]
		var waiting : Integer <- 0
		var wall : Condition <- Condition.create
		operation print[s : String] -> []
			const name <- "[B:" || waiting.asString || "/"
					|| size.asString || "]"
			stdout.putString[ name || ": " || s || "\n"]
		end print

		operation wake[] -> []
			self.print["Wall is breached!"]
			assert waiting == size
			loop
				exit when waiting == 0
				signal wall
				waiting <- waiting - 1
			end loop
			self.print["Wall is strong again"]
		end wake

		export operation cross[] -> []
			self.print["trying to cross"]
			waiting <- waiting + 1
			if waiting < size then
				self.print["waiting"]
				wait wall
			else
				self.wake
			end if
		end cross
	end Barrier

	const wallCapacity : Integer <- 4
	const TheWall <- Barrier.create[wallCapacity]

	const WhiteWalker <- class WhiteWalker [id : Integer]
	% this is an object tryng to breach the wall
		var name : String <- "[W: " || id.asString || " ]"
		operation print[s : String] -> []
			stdout.putstring[name || ": " || s || "\n"]
		end print
		export operation attack[] -> []
			self.print["attack!!!"]
			TheWall.cross
		end attack
	end WhiteWalker

	const WhiteWalkerSpawner <- class WhiteWalkerSpawner[id : Integer]
	% this is object spawning processes breaching the wall,
	% each fo the white walkers tries to attack the wall 4 times
		process
			const e <- WhiteWalker.create[id]
			var j : Integer <- 0
			loop 
				exit when j == 4 
				e.attack
				j <- j + 1
			end loop
		end process
	end WhiteWalkerSpawner

	process 
	% here white walker spawner is created, there are 4 spawners,i
	% so there will be 4 white walkers
		var i : Integer <- 0
		loop 
			exit when i == 4
			const ww <- WhiteWalkerSpawner.create[i]
			i <- i + 1
		end loop
	end process

end barrierExample
