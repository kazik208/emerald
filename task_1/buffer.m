const bufferExample <- object bufferExample
	const Buffer <- monitor class Buffer[size : Integer]
		var count : Integer <- 0
		var get_q : Condition <- Condition.create
		var put_q : Condition <- Condition.create
		const store <- Array.of[Integer].empty

		operation print[s : String] -> []
				stdout.putString["[B] "|| s || "\n"]
		end print

		export operation put[i : Integer] -> []
			if count == size then
				wait put_q
			end if
			count <- count + 1
			self.print["insert " || i.asString]
			store.addUpper[i]
			signal get_q
		end put

		export operation get[] -> [r : Integer]
			if count == 0 then
				wait get_q
			end if
			count <- count - 1
			r <- store.removeLower
			self.print["remove " || r.asString]
			signal put_q
		end get
	end Buffer

	const buff <- Buffer.create[2]

	const Producer <- class Producer
		operation print[s : String] -> []
				stdout.putString["[P] "|| s || "\n"]
		end print

		process
			var i : Integer <- 1
			loop
				exit when i > 30
				if i # 3 == 0 then
					(locate self).delay[Time.create[0,100]]
				end if
				self.print["produced: " || i.asString]
				buff.put[i]
				i <- i + 1
			end loop
		end process
	end Producer
		
	const Consumer <- class Consumer
		operation print[s : String] -> []
				stdout.putString["[C] "|| s || "\n"]
		end print

		process
			var i : Integer <- 1
			loop
				exit when i > 30
				if i # 5 == 0 then
					(locate self).delay[Time.create[0,100]]
				end if
				var j : Integer <- buff.get
				self.print["consuming: " || j.asString]
				i <- i + 1
			end loop
		end process
	end Consumer

	process
		const p <- Producer.create
		const c <- Consumer.create
	end process

end bufferExample
