% collect times from different nodes and display them
const TimeCollector <- object TimeCollector
  process
    const home <- locate self
    var there :     Node
    var startTime, diff : Time
    var all : NodeList
    var theElem :NodeListElement
    var stuff : Real
    const times <- array.of[String].empty

    home$stdout.PutString["Starting on " || home$name || "\n"]
    all <- home.getActiveNodes
    home$stdout.PutString[(all.upperbound + 1).asString || " nodes active.\n"]
    times.addUpper[home.getTimeOfDay.asDate || "\t[" || home$name || "]"]

    % visit all the nodes
    for i : Integer <- 1 while i <= all.upperbound by i <- i + 1
      there <- all[i]$theNode
      move TimeCollector to there
      times.addUpper[there.getTimeOfDay.asDate || "\t[" || there$name || "]"]
      there$stdout.PutString["TimeCollector was here\n"]
    end for

    % print results
    move TimeCollector to home
    for i : Integer <- times.lowerbound while i <= all.upperbound by i <- i + 1
      home$stdout.PutString[times[i] || "\n"]
    end for
  end process
end TimeCollector
