const TimeSync <- object TimeSync

	const TimeKeeper <- monitor class TimeKeeper
			const times <- array.of[Time].empty
			const diffs <- array.of[Time].empty
			var size : Integer <- 1
			var wall : Condition <- Condition.create
			var Mean, Base : Time
			
			export operation SetTime[timestamp : Time] -> []
				% add another result
				times.addUpper[timestamp]
				if times.upperbound + 1 == size then
					signal wall
				end if
			end SetTime
			
			export operation setSize[new_size : Integer] -> []
				% set expected number of agents
				size <- new_size
			end setSize

			operation calcDiffs[] -> []
				% calculate time differences
				base <- times[0]
				for i : Integer <- 0 while i <= times.upperbound by i <- i + 1
					diffs.addUpper[times[i] - base]
				end for
			end calcDiffs

			operation calcMean[] -> []
				% calculations are done on differences to avoid overflow
				Mean <- diffs[0]
				for i : Integer <- 1 while i <= times.upperbound by i <- i + 1
					Mean <- Mean + diffs[i]
				end for
				Mean <- Mean / (times.upperbound + 1)
				% now add base to get proper result
				Mean <- Mean + base
			end calcMean

			export operation calculate[] -> []
				% wait for all agents to complete
				if times.upperbound + 1 < size then
					wait wall
				end if
				self.calcDiffs
				self.calcMean
			end calculate
				
			
			export operation print[] -> []
				const home <- locate self
				% print timestamps
				home$stdout.PutString["id\t" ]
				home$stdout.PutString["date\t\t\t\t" ]
				home$stdout.PutString["offset from base\n" ]
				for i : Integer <- 0 while i <= times.upperbound by i <- i + 1
					home$stdout.PutString[i.asString || "\t" ]
					home$stdout.PutString[times[i].asDate || "\t" ]
					home$stdout.PutString[diffs[i].asString || "\n" ]
				end for
				home$stdout.PutString["Mean:\t" || Mean.asDate || "\t" || (Mean - Base).asString || "\n" ]
			end print
	
	end TimeKeeper

	const Clocksmith <- TimeKeeper.create

	const Agent <- object Agent
	% agents are asynchronously sending the time from their node to the clocksmith
		export operation create[there : Node] -> []
			object AnAgent
				process
					move self to there
					const time <- there.getTimeofDay
					Clocksmith.setTime[time]
				end process
			end AnAgent
		end create
	end Agent

	process
		const home <- locate self
		var there :	Node
		var all : NodeList

		home$stdout.PutString["Starting on " || home$name || "\n"]
		all <- home.getActiveNodes
		home$stdout.PutString[(all.upperbound + 1).asString || " nodes active.\n"]
		Clocksmith.setSize[all.upperbound + 1]
		% plant agents
		for i : Integer <- 0 while i <= all.upperbound by i <- i + 1
			there <- all[i]$theNode
			agent.create[there]
		end for

		% do the calculations
		Clocksmith.calculate

		% print stuff
		home$stdout.PutString["Back home." || "\n"]
		Clocksmith.print
	end process
end TimeSync
