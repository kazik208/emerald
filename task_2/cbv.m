% finding breakeven point for call by visit vs by remote reference
const CallByVisit <- object CallByVisit

	const TesterCl <- class Tester
		export operation print[s : String] -> []
			const place <- locate self
			const name <- "Tester@" || place$name 
			CallByVisit.log[name, s]
		end print

		export operation test[data : Array.of[Character],
				calls : Integer ] -> []
			% make exactly calls(number) calls to data array.
			const tmp <- Array.of[Character].empty
    			for j : Integer <- 0 while j < calls by j <- j + 1
				tmp.addUpper[data[j]]
			end for
		end test
	end Tester

	export operation log[who : String, what : String] -> []
		stdout.putString["[" || who || "]: " || what || "\n"]
	end log

	process
		const home <- locate self
		const tester <- TesterCl.create
		% sizes in btes which will be tested
		const sizes <- {100, 500, 1000, 2000, 10000, 100000}
		% number of accesses tested
		const accesses <- {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 40}
		% array used for references
		const arr <- Array.of[Character].empty

		var there :     Node
		var startTime, diff : Time
		var all : NodeList
		var theElem :NodeListElement
		
		home$stdout.PutString["Starting on " || home$name || "\n"]
		all <- home.getActiveNodes
		home$stdout.PutString[(all.upperbound + 1).asString
				|| " nodes active.\n"]

		for j : Integer <- 0 while j <= sizes.upperbound by j <- j + 1
			var size : Integer <- sizes[j]

			% fill array up to a given size
			var x : Integer <- arr.upperbound
			loop 
				exit when x = size
				arr.addUpper['a']
				x <- x+1
			end loop 
			
			self.log["data size (bytes) ", (arr.upperbound).asString]

			% test different remote machines
			for i : Integer <- 1 while i <= all.upperbound by i <- i + 1
				there <- all[i]$theNode
				move tester to there
				tester.print["accesses || no visit || visit "]
			
				% test different number of calls
				for k : Integer <- accesses.lowerbound while k <= accesses.upperbound by k <- k + 1
			 		var accesses_count : Integer
			 		var print_str : String
			
			 		accesses_count <- accesses[k]
			 		print_str <- accesses_count.asString || "\t\t"
			
					% remote reference test
			 		startTime <- home.getTimeOfDay
			 		tester.test[arr, accesses_count]
			 		diff <- home.getTimeOfDay - startTime
			 		print_str <- print_str || diff.asString || "\t"

					% call by visit
			 		startTime <- home.getTimeOfDay
			 		move arr to there
			 		tester.test[arr, accesses_count]
			 		move arr to home
			 		diff <- home.getTimeOfDay - startTime
			 		print_str <- print_str || diff.asString

			 		tester.print[print_str]
				end for
			end for
		end for
	end process
end CallByVisit
